#!/bin/bash

# Prerequisites for PetaLinux
sudo -E apt-get update
sudo -E apt-get install -y --no-install-recommends --no-install-suggests \
    binfmt-support \
    debootstrap \
    gcc-multilib \
    g++-multilib \
    qemu-user-static
sudo -E apt-get clean
sudo -E rm -rf /var/lib/apt/lists/*

#    gcc-arm-linux-gnueabihf \
#    gcc-aarch64-linux-gnu \
#    g++-arm-linux-gnueabihf \
#    g++-aarch64-linux-gnu \
#    gdb-multiarch \
