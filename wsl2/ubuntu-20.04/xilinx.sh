#!/bin/bash

# Create Install Directory
sudo mkdir -p /tools/Xilinx
sudo chown $(id -u):$(id -g) /tools/Xilinx

# Prerequisites for Vitis
sudo -E apt-get update
sudo -E apt-get install -y --no-install-recommends --no-install-suggests \
    libswt-gtk-4-jni \
    libtinfo5
sudo -E apt-get clean
sudo -E rm -rf /var/lib/apt/lists/*

# Installing Vitis
bash ./Xilinx_Unified_2022.1_0420_0327_Lin64.bin

# Prerequisites for PetaLinux
sudo dpkg --add-architecture i386
sudo -E apt-get update
sudo -E apt-get install -y --no-install-recommends --no-install-suggests \
    autoconf \
    gcc-multilib \
    g++-multilib \
    libtool \
    net-tools \
    texinfo \
    xterm \
    zlib1g-dev \
    zlib1g:i386
sudo -E apt-get clean
sudo -E rm -rf /var/lib/apt/lists/*

# Installing PetaLinux
mkdir -p /tools/Xilinx/PetaLinux/2022.1
# PetaLinuxは一般ユーザでインストール
bash ./petalinux-v2022.1-04191534-installer.run -d /tools/Xilinx/PetaLinux/2022.1

# dpkg-reconfigure dash で /bin/sh を dash -> bash に設定
