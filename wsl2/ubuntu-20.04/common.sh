#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
_PWD=$(pwd)
WORKDIR=$(readlink -e $0 | xargs dirname)
ROOT=$(readlink -e ${WORKDIR}/../..)

cd ${WORKDIR}

sudo -E mv /etc/apt/sources.list /etc/apt/sources.list.org
sed -e 's/http:\/\/archive\.ubuntu\.com/http:\/\/jp\.archive\.ubuntu\.com/g' /etc/apt/sources.list.org > ./sources.list
sudo -E mv ./sources.list /etc/apt/sources.list
sudo rm -f /etc/apt/sources.list.org

echo -e  "APT::Install-Suggests 0;\nAPT::Install-Recommends 0;" | sudo tee /etc/apt/apt.conf.d/00-no-install-recommends

sudo -E apt-get update
sudo -E apt-get install -y --no-install-recommends --no-install-suggests \
    apt-utils \
    ca-certificates \
    curl \
    file \
    less \
    sudo \
    tmux \
    tzdata \
    vim-tiny \
    wget \
    zsh

sudo -E apt-get install -y --no-install-recommends --no-install-suggests \
    build-essential \
    clang \
    cmake \
    gdb \
    git \
    lldb \
    make \
    python-is-python3 \
    python3 \
    python3-pip \
    python3-venv

sudo -E apt-get install -y --no-install-recommends --no-install-suggests \
    x11-xkb-utils

curl -fsSL get.docker.com -o ./get-docker.sh \
    && bash ./get-docker.sh \
    && sudo usermod -aG docker ${USER}
rm -f ./get-docker.sh

sudo apt-get clean
sudo rm -rf /var/lib/apt/lists/*

python -m pip install pip --user
rm -rf ${HOME}/.cache/pip

ln -nfs $(readlink -e ${ROOT}/tmux/.tmux.conf) ${HOME}/
ln -nfs $(readlink -e ${ROOT}/zsh/.zshenv)     ${HOME}/
ln -nfs $(readlink -e ${ROOT}/zsh/.zshrc)      ${HOME}/

unset DEBIAN_FRONTEND
cd ${_PWD}

# 日本語版Windowsで英語キーボードを使うとWSLのGUI上で日本語レイアウトになる場合は以下を実行
setxkbmap -model pc104 -layout us
