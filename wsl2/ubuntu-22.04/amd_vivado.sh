#!/bin/bash

# Create Install Directory
sudo mkdir -p /tools
sudo chown $(id -u):$(id -g) /tools

# Prerequisites for installing Vitis
sudo -E apt-get update
sudo -E apt-get install -y --no-install-recommends --no-install-suggests \
    libswt-gtk-4-jni \
    libxtst6

# Prerequisites for running Vitis
sudo -E apt-get install -y --no-install-recommends --no-install-suggests \
    libncurses5 \
    libtinfo5 \
    locales-all

# Prerequisites for running Vitis HLS
sudo -E apt-get install -y --no-install-recommends --no-install-suggests \
    gcc-multilib \
    g++-multilib

sudo -E apt-get clean
sudo -E rm -rf /var/lib/apt/lists/*

# Installing Vitis
# bash ./Xilinx_Unified_2022.1_0420_0327_Lin64.bin
# bash ./FPGAs_AdaptiveSoCs_Unified_2023.2_1013_2256_Lin64.bin
