#!/bin/zsh
#-*- coding: utf-8 -*-

umask 022
KEYTIMEOUT=1

# keybind - emacs like
bindkey -d
bindkey -e
bindkey ' ' magic-space
bindkey '^@'   set-mark-command
bindkey '^A'   beginning-of-line
bindkey '^B'   backward-char
bindkey '^D'   delete-char-or-list
bindkey '^E'   end-of-line
bindkey '^F'   forward-char
bindkey '^G'   end-of-buffer-or-history
bindkey '^K'   kill-line
#bindkey '^N'    down-line-or-search
bindkey '^O'   accept-line-and-down-history
#bindkey '^P'    up-line-or-search
bindkey '^R'   history-incremental-search-backward
bindkey '^T'   transpose-chars
bindkey '^W'   backward-delete-word
bindkey '^_'   undo

# colors
autoload colors
colors

# history
HISTSIZE=100000
SAVEHIST=1000000
HISTFILE=$HOME/.zsh_history

setopt APPEND_HISTORY        #
setopt BANG_HIST             # !番号 でヒストリを手繰る
setopt EXTENDED_HISTORY      # ヒストリの開始と終了を記録
setopt NO_HIST_BEEP
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_ALL_DUPS  # ヒストリがダブったら古いのを消す
setopt HIST_IGNORE_DUPS      # 重複を記録しない
setopt HIST_IGNORE_SPACE     # 最初がスペースの文字列をヒストリに入れない
setopt HIST_NO_FUNCTIONS
setopt HIST_REDUCE_BLANKS    # ヒストリはスペース詰めて記録
setopt HIST_SAVE_NO_DUPS     # historyコマンドを記録に残さない
setopt SHARE_HISTORY         # share history among zshs

autoload -U history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
bindkey '^N'  history-beginning-search-forward-end
bindkey '^P'  history-beginning-search-backward-end
bindkey '^N' down-line-or-search
bindkey '^P' up-line-or-search

# aliases
alias ls='ls -F'
alias mv='nocorrect \mv -i'
alias cp='nocorrect \cp -i'
alias rm='\rm -i'

alias -g A='|awk'
alias -g C='|cat'
alias -g G='|grep'
alias -g H='|head'
alias -g L='|lv'
alias -g S='|sed'
alias -g T='|tail'

#alias emacs='emacsclient -a ""'
#alias e='emacsclient -nw -a ""'
#alias ekill='emacsclient -e "(kill-emacs)"'
#alias elcc='\emacs -batch -L ./ -f batch-byte-compile'

alias s='source'
alias x='exit'
alias p='pushd'
alias pp='popd'
alias j='jobs'

alias -s txt=vi
alias -s md=vi
alias -s rst=vi
alias -s tex=vi

alias -s c=vi
alias -s cc=vi
alias -s cpp=vi
alias -s h=vi
alias -s hpp=vi
alias -s py=vi
alias -s v=vi
alias -s vh=vi
alias -s sv=vi

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

if [ -f ~/.zsh_aliases ]; then
    . ~/.zsh_aliases
fi

# predict
#autoload predict-on
#predict-on

# completion
autoload -Uz compinit
compinit -u

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# prompt
PROMPT="%E%n@%m[%~]
%# "

PROMPT2="%_%# "
SPROMPT="%r is correct? [No/Yes/Abort/Edit] :"
#PROMPT="%E%n@%m%# "
#RPROMPT="[%~]"

### Zsh Options ##########################################################
#       See manual zshoptions(1).

# Changing Directories
setopt   AUTO_CD               # ディレクトリ名だけで移動
setopt   NO_AUTO_PUSHD         # cdでもpushdする
setopt   CDABLE_VARS           # cdできる値のみ表示(cd hogeで~hogeにいったりする)
setopt   PUSHD_IGNORE_DUPS     # 同じディレクトリをpushdしない

# Completion
setopt   ALWAYS_TO_END         # 補完したら単語の終わりに飛ぶ(不要？)
setopt   AUTO_LIST             #
setopt   AUTO_MENU             #
setopt   COMPLETE_IN_WORD      # 単語の中間でも補完
setopt   NO_GLOB_COMPLETE      # unsetしていると * が一気に候補全部になる
setopt   NO_LIST_BEEP          # 補完の度にピーピー言わない
setopt   LIST_PACKED           # 候補を密に表示

# Expansion and Globbing
setopt   EQUALS                # =<command> とすると<command>のパスが展開
setopt   EXTENDED_GLOB         # #~^ をメタキャラクタにする
setopt   GLOB_DOTS             # .で始まるファイルを*で扱う
setopt   MAGIC_EQUAL_SUBST     # 引数のa=bのbの部分を展開(~等)
setopt   NUMERIC_GLOB_SORT     # ファイル名を数値順に展開

# Input/Output
setopt   NO_CLOBBER            # ファイルの上書きを禁止
setopt   CORRECT               # コマンドのスペルミスを訂正
setopt   CORRECT_ALL           # 引数も訂正
setopt   NO_FLOW_CONTROL       # フローコントロールしない
setopt   IGNORE_EOF            # avoid logout due to Ctrl-D
setopt   INTERACTIVE_COMMENTS  # シェル中にコメントを許す
setopt   PRINT_EIGHT_BIT       # 補完候補に日本語表示

# Job Control
setopt   AUTO_RESUME           # 1文字でジョブを手前に持って来る
setopt   NO_BG_NICE            # バックグラウンドジョブにniceをかけない
setopt   NO_HUP                # ログアウト時にプロセスをHUPしない
setopt   LONG_LIST_JOBS        # 長いフォーマットでジョブをリスト(不要？)
setopt   NOTIFY                # バックグラウンドジョブの状態を出力

# Prompting
setopt   PROMPT_SUBST          # プロンプトに変数などを利用可能に

# Scripts and Functions
setopt   FUNCTION_ARGZERO      # $0を関数、スクリプト名にする

# Zle
setopt   ZLE                   # zshのラインエディタを使う

# for local configuration

if [ -e ${HOME}/.zshrc.local ] ; then
    source ${HOME}/.zshrc.local
fi
