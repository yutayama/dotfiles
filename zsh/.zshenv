#!/bin/zsh
#-*- coding:utf-8 -*-

export HOST=$(hostname)
export EDITOR=vi
export PAGER=less

# for WSL2 without WSLg
if [ $(uname -r | grep WSL2) ] && [ ! "x$(which wslg.exe)" != "x" ]; then
    export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}'):0.0
fi

# for snap packages
if [ -d /snap/bin ]; then
    export PATH=/snap/bin:$PATH
fi

# for pip packages
if [ -d $HOME/.local/bin ]; then
    export PATH=$HOME/.local/bin:$PATH
fi

# for local configuration
if [ -e ${HOME}/.zshenv.local ] ; then
    source ${HOME}/.zshenv.local
fi
