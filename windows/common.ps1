# install

wsl --install

# winget install Google.Chrome
# winget install Microsoft.Office
# winget install Dropbox.Dropbox

winget install nathancorvussolis.corvusskk
# 辞書のダウンロード
# - skkjisyo.lダウンロード  from http://openlab.ring.gr.jp/skk/wiki/wiki.cgi?page=SKK%BC%AD%BD%F1
# - [設定] > [辞書1]
#   - [ファイル追加] & [取込]
# キーバインド設定 (SHIFT+SPACEでON/OFFするように追加)
# - [設定] > [キー0]
#   - SHIFT + 仮想キー0x20(SPACE) を ON / OFF それぞれに追加 (更新ボタンで追加)
winget install 7zip.7zip
winget install Microsoft.VisualStudioCode
# winget install Microsoft.VisualStudio.2022.Community
# winget install Kitware.CMake
# winget install Git.Git
# winget install TortoiseGit.TortoiseGit

winget install Apple.iTunes
winget install iCloud
#winget install AgileBits.1Password -> 8のみなので7を手動で

# caps -> ctrl

## regedit
## 
## HKEY_LOCAL_MACHINE -> SYSTEM -> CurrentControlSet -> Control -> Keyboard Layout
## 
## Scancode Map
## 00 00 00 00 00 00 00 00
## 02 00 00 00 1d 00 3a 00
## 00 00 00 00

# WSL2

## wsl --install
## ubuntu re-install
## 設定からuninstall
## wsl --install -d Ubuntu
